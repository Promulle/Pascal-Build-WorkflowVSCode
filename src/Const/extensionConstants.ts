export const EXTENSION_PREFIX  : string = 'pbw';
export const EXTENSION_PROJECT : string = '.dpr';
export const EXTENSION_PACKAGE : string = '.dpk';

export const COMMAND_BUILD_CURRENT : string = 'extension.buildCurrent';
export const COMMAND_BUILD_ALL     : string = 'extension.buildAll';
export const COMMAND_INSERT_LINE   : string = 'extension.lineInsert';

export const SETTING_ENABLE_LOGGING              : string = 'EnableLogging';
export const SETTING_ENABLE_LOGGING_DEFAULT      : boolean = false;
export const SETTING_BASE_LOG_PATH               : string = 'BaseLogPath';
export const SETTING_BASE_LOG_PATH_DEFAULT       : string = './log';
export const SETTING_BUILD_SCRIPT_SINGLE         : string = 'buildScript';
export const SETTING_BUILD_SCRIPT_SINGLE_DEFAULT : string = 'build.bat';
export const SETTING_BUILD_SCRIPT_ALL            : string = 'buildAllScript';
export const SETTING_BUILD_SCRIPT_ALL_DEFAULT    : string = 'buildAll.bat';
export const SETTING_LINE_FUNCTION_START         : string = 'BeforeFunctionLine';
export const SETTING_LINE_FUNCTION_START_DEFAULT : string = '//Effect: ';
export const SETTING_LINE_FUNCTION_END           : string = 'AfterFunctionLine';
export const SETTING_LINE_FUNCTION_END_DEFAULT   : string = '//---------------------------------';

export const INFO_BUILD_NOT_FOUND : string = 'Could not find or build project of ';
export const INFO_BUILD_EXECUTED  : string = 'Executed build file for: '; 
export const INFO_BUILD_ALL       : string = 'Called buildscript for building everything.';

export const ERROR_BUILD_SINGLE : string = 'Error building project: ';
export const ERROR_BUILD_ALL    : string = 'Error occured during building.';

export const PASCAL_DPK_USING : string = 'uses';
export const PASCAL_DPR_USING : string = 'contains';
export const PASCAL_END       : string = 'end;';
export const PASCAL_FUNCTION  : string = 'function';
export const PASCAL_PROCEDURE : string = 'procedure';

export const UNDEFINED : string = 'undefined';