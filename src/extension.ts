'use strict';
import * as vscode        from 'vscode';

//imports for custom files
import * as extUtils         from './Utils/ExtUtils';
import * as extConst         from './Const/extensionConstants';
import * as extConfig        from './Utils/ExtConfig';
import * as extBuildService  from './Services/BuildService';
import * as extInsertService from './Services/InsertLineService';

let appConfig = new extConfig.ExtConfig();
let appUtils = new extUtils.ExtUtils(appConfig.GetSetting(extConst.SETTING_BASE_LOG_PATH, extConst.SETTING_BASE_LOG_PATH_DEFAULT));
let appBuildService = new extBuildService.BuildService(appConfig, appUtils);
let appInsertService = new extInsertService.InsertLineService(appConfig);

export function activate(context: vscode.ExtensionContext) {
    appUtils.Logger.EnableLogging = appConfig.GetSetting(extConst.SETTING_ENABLE_LOGGING, extConst.SETTING_ENABLE_LOGGING_DEFAULT);
    let disposable = vscode.commands.registerCommand(extConst.COMMAND_BUILD_CURRENT, () => {
        appBuildService.CallSingleBuild();
        
    });

    let secondCommand = vscode.commands.registerCommand(extConst.COMMAND_BUILD_ALL, () => {
        appBuildService.CallAllBuild();
    });

    let insertCommand = vscode.commands.registerCommand(extConst.COMMAND_INSERT_LINE, () => {
        appInsertService.InsertLine();
    });
    context.subscriptions.push(disposable);
    context.subscriptions.push(secondCommand);
    context.subscriptions.push(insertCommand);
}


// this method is called when your extension is deactivated
export function deactivate() {
}