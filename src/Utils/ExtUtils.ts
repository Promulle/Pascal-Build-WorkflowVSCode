import * as fs from 'fs';
import * as os from 'os';
import * as path from 'path';

import * as log from './ExtLog';

export class ExtUtils {
    public Logger: log.ExtLog;
    public constructor(i_LoggerFile: string) {
        this.Logger = new log.ExtLog(i_LoggerFile);
    }
    //reads from file to array of string
    ReadFileToArray(i_FilePath: string): string[] {
        let lines: string[] = [];
        let read: string = fs.readFileSync(i_FilePath, 'utf8');
        lines = read.split(os.EOL);
        return lines;
    
    }
    //capitalizes first letter
    public static CapitalizeFirstLetter(i_Value: string): string {
        return i_Value.charAt(0).toUpperCase() + i_Value.slice(1);
    }
    public static GetFilePaths(i_Directory: string): string[] {
        let fileNames = fs.readdirSync(i_Directory);
        let res: string[] = [];
        let i: number;
        for(i = 0; i < fileNames.length; i++) {
            res.push(i_Directory + path.sep + fileNames[i]);
        }
        return res;
    }
}