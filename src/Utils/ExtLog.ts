import * as fs from 'fs';
import * as os from 'os';

const MESSAGE_START: string = 'LoggingSession started';
const FILE_EXTENSION: string = '.txt';

export class ExtLog {
    private m_BaseLogFilePath: string;
    private m_LogFilePath: string;
    private m_EnableLogging: boolean;
    get EnableLogging(): boolean{
        return this.m_EnableLogging;
    }
    set EnableLogging(i_Value: boolean) {
        this.m_EnableLogging = i_Value;
    }
    //base path for LogFile
    get BaseLogFilePath(): string {
        return this.m_BaseLogFilePath;
    }
    //actual filepath of logFile
    get LogFilePath(): string {
        return this.m_LogFilePath;
    }
    constructor(i_FilePath: string) {
        this.m_EnableLogging = false;
        this.m_BaseLogFilePath = i_FilePath;
        this.m_LogFilePath = this.BaseLogFilePath + ExtLog.GenerateLogTime() + FILE_EXTENSION;
        this.Log(MESSAGE_START);
    }
    private static GenerateLogTime(): string {
        let date = new Date();
        return date.getHours() + '_' + date.getMinutes() + '_' + date.getSeconds() + '-' + date.getDate() + '_' + (date.getMonth() + 1) + '_' + date.getFullYear();
    }
    public Log(i_Message: string) {
        if (this.EnableLogging === true){
            fs.appendFileSync(this.LogFilePath, i_Message +  os.EOL);
        }
    }
}