import * as vscode from 'vscode';
import * as extConst from '../Const/extensionConstants';

export class ExtConfig {
    public GetSetting<T>(i_Name: string, i_DefaultValue: T): T{
        return vscode.workspace
        .getConfiguration(extConst.EXTENSION_PREFIX)
        .get<T>(i_Name, i_DefaultValue);
    }
}