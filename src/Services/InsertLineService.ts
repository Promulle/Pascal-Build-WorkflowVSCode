import * as vscode from 'vscode';

import * as extConfig from '../Utils/ExtConfig';
import * as extConst  from '../Const/extensionConstants';

export class InsertLineService {
    private m_Config            : extConfig.ExtConfig;
    private m_EndFunctionLine   : string;
    private m_BeginFunctionLine : string;
    public constructor(i_Config: extConfig.ExtConfig) {
        this.m_Config = i_Config;
        this.m_BeginFunctionLine = this.m_Config.GetSetting(extConst.SETTING_LINE_FUNCTION_START, extConst.SETTING_LINE_FUNCTION_START_DEFAULT);
        this.m_EndFunctionLine   = this.m_Config.GetSetting(extConst.SETTING_LINE_FUNCTION_END, extConst.SETTING_LINE_FUNCTION_END_DEFAULT);
    }
    public InsertLine(): void{
        if (typeof(vscode.window.activeTextEditor) != extConst.UNDEFINED) {
            let currentlyOpenEditor = vscode.window.activeTextEditor as vscode.TextEditor;
            let lineToInsert = this.GetLine(currentlyOpenEditor);
            currentlyOpenEditor.edit(edit => edit.insert(currentlyOpenEditor.selection.start, lineToInsert));
        }
    }
    private GetLine(i_Editor: vscode.TextEditor): string {
        let lineBefore = this.GetLineBeforeSelection(i_Editor);
        let lineAfter = this.GetLineAfterSelection(i_Editor);
        if (lineBefore.includes(extConst.PASCAL_END)){
            return this.m_EndFunctionLine;
        }
        if (lineAfter.includes(extConst.PASCAL_FUNCTION) || lineAfter.includes(extConst.PASCAL_PROCEDURE)){
            return this.m_BeginFunctionLine;
        }
        return "";
    }
    private GetLineBeforeSelection(i_Editor : vscode.TextEditor): string{
        if (i_Editor.selection.start.line - 1 > -1){
            return i_Editor.document.lineAt(i_Editor.selection.start.line - 1).text;
        }
        return "";
    }
    private GetLineAfterSelection(i_Editor : vscode.TextEditor): string{
        if (i_Editor.selection.start.line + 1 < i_Editor.document.lineCount){
            return i_Editor.document.lineAt(i_Editor.selection.start.line + 1).text;
        }
        return "";  
    }
}