"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode = require("vscode");
const extConst = require("../Const/extensionConstants");
class InsertLineService {
    constructor(i_Config) {
        this.m_Config = i_Config;
        this.m_BeginFunctionLine = this.m_Config.GetSetting(extConst.SETTING_LINE_FUNCTION_START, extConst.SETTING_LINE_FUNCTION_START_DEFAULT);
        this.m_EndFunctionLine = this.m_Config.GetSetting(extConst.SETTING_LINE_FUNCTION_END, extConst.SETTING_LINE_FUNCTION_END_DEFAULT);
    }
    InsertLine() {
        if (typeof (vscode.window.activeTextEditor) != extConst.UNDEFINED) {
            let currentlyOpenEditor = vscode.window.activeTextEditor;
            let lineToInsert = this.GetLine(currentlyOpenEditor);
            currentlyOpenEditor.edit(edit => edit.insert(currentlyOpenEditor.selection.start, lineToInsert));
        }
    }
    GetLine(i_Editor) {
        let lineBefore = this.GetLineBeforeSelection(i_Editor);
        let lineAfter = this.GetLineAfterSelection(i_Editor);
        if (lineBefore.includes(extConst.PASCAL_END)) {
            return this.m_EndFunctionLine;
        }
        if (lineAfter.includes(extConst.PASCAL_FUNCTION) || lineAfter.includes(extConst.PASCAL_PROCEDURE)) {
            return this.m_BeginFunctionLine;
        }
        return "";
    }
    GetLineBeforeSelection(i_Editor) {
        if (i_Editor.selection.start.line - 1 > -1) {
            return i_Editor.document.lineAt(i_Editor.selection.start.line - 1).text;
        }
        return "";
    }
    GetLineAfterSelection(i_Editor) {
        if (i_Editor.selection.start.line + 1 < i_Editor.document.lineCount) {
            return i_Editor.document.lineAt(i_Editor.selection.start.line + 1).text;
        }
        return "";
    }
}
exports.InsertLineService = InsertLineService;
//# sourceMappingURL=InsertLineService.js.map