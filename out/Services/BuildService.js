"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode = require("vscode");
const path = require("path");
const child_process = require("child_process");
//imports for custom files
const extConst = require("../Const/extensionConstants");
const extUtils = require("../Utils/ExtUtils");
class BuildService {
    constructor(config, utils) {
        this.m_Config = config;
        this.m_Utils = utils;
        this.m_Logger = utils.Logger;
    }
    //Calls all build script
    CallAllBuild() {
        let bResult = true;
        let buildFileName = this.m_Config.GetSetting(extConst.SETTING_BUILD_SCRIPT_ALL, extConst.SETTING_BUILD_SCRIPT_ALL_DEFAULT);
        try {
            child_process.execFileSync(buildFileName);
        }
        catch (_a) {
            bResult = false;
        }
        this.m_Logger.Log('Called buildscript: ' + buildFileName);
        vscode.window.showInformationMessage(extConst.INFO_BUILD_ALL);
        if (!bResult) {
            vscode.window.showErrorMessage(extConst.ERROR_BUILD_ALL);
        }
    }
    //Builds single project from selected tab
    CallSingleBuild() {
        if (typeof (vscode.window.activeTextEditor) != extConst.UNDEFINED) {
            let currentlyOpenEditor = vscode.window.activeTextEditor;
            let filePath = extUtils.ExtUtils.CapitalizeFirstLetter(currentlyOpenEditor.document.fileName);
            this.m_Logger.Log('Currently selected file: ' + filePath);
            if (this.IsProjectFile(filePath)) {
                //if filepath is projetc just build that
                this.BuildProject(filePath);
                vscode.window.showInformationMessage('Projectfile ' + path.basename(filePath) + ' built.');
            }
            else {
                //else find corresponding project file
                let usingFileName = path.basename(filePath);
                this.m_Logger.Log('Filename to search for: ' + usingFileName);
                let dirPath = path.dirname(filePath);
                this.m_Logger.Log('Directory path of file: ' + dirPath);
                let files = extUtils.ExtUtils.GetFilePaths(dirPath);
                let built = false;
                for (let file of files) {
                    if (this.CheckAndBuildIfFittingProject(usingFileName, file)) {
                        built = true;
                        break;
                    }
                }
                ;
                if (!built) {
                    vscode.window.showInformationMessage(extConst.INFO_BUILD_NOT_FOUND + path.basename(filePath));
                }
            }
        }
    }
    //check if i_FilePath is a project, the correct project and build if correct
    //returns true if project has been built
    CheckAndBuildIfFittingProject(i_UsingFile, i_FilePath) {
        if (this.IsProjectFile(i_FilePath)) {
            if (this.SearchProjectFile(i_UsingFile, i_FilePath)) {
                this.BuildProject(i_FilePath);
                return true;
            }
            else {
                this.m_Logger.Log('File: ' + i_FilePath + ' is not the correct project.');
            }
        }
        return false;
    }
    IsProjectFile(i_FilePath) {
        let ext = path.extname(i_FilePath);
        this.m_Logger.Log('Comparing extension with project extensions: ' + ext);
        return ext == extConst.EXTENSION_PACKAGE || ext == extConst.EXTENSION_PROJECT;
    }
    //searches whether i_ProjectFile is the correct project to build
    SearchProjectFile(i_UsingFile, i_ProjectFile) {
        let lines = this.m_Utils.ReadFileToArray(i_ProjectFile);
        let isInUses = false;
        let i = 0;
        for (i = 0; i < lines.length; i++) {
            if (isInUses) {
                if (this.IsCorrectUsing(lines[i], i_UsingFile)) {
                    return true;
                }
            }
            if (lines[i].includes(extConst.PASCAL_DPK_USING) || lines[i].includes(extConst.PASCAL_DPR_USING)) {
                isInUses = true;
            }
        }
        return false;
    }
    BuildProject(i_ProjectFile) {
        let buildFileName = this.m_Config.GetSetting(extConst.SETTING_BUILD_SCRIPT_SINGLE, extConst.SETTING_BUILD_SCRIPT_SINGLE_DEFAULT);
        //build project, call buildscript for project
        let arg = i_ProjectFile;
        let bResult = false;
        try {
            child_process.execFileSync(buildFileName, [arg]);
            bResult = true;
        }
        catch (_a) {
            bResult = false;
        }
        this.m_Logger.Log('Called buildscript: ' + buildFileName + ' ' + i_ProjectFile);
        vscode.window.showInformationMessage(extConst.INFO_BUILD_EXECUTED + i_ProjectFile);
        if (!bResult) {
            vscode.window.showErrorMessage(extConst.ERROR_BUILD_SINGLE + i_ProjectFile);
        }
    }
    //check using
    IsCorrectUsing(i_UsingLine, i_ExpectedFile) {
        let lineSplit = i_UsingLine.split('in');
        this.m_Logger.Log('Using in ' + i_UsingLine);
        this.m_Logger.Log('Searching for: ' + i_ExpectedFile);
        if (lineSplit != null && lineSplit.length == 2) {
            this.m_Logger.Log('Name: ' + lineSplit[0]);
            //remove all not needed characters
            let fileNameInUsingPath = path.basename(lineSplit[1].trim())
                .replace(/'/g, '')
                .replace(/,/g, '')
                .replace(/;/g, '');
            this.m_Logger.Log('File: ' + fileNameInUsingPath);
            if (fileNameInUsingPath == i_ExpectedFile) {
                return true;
            }
        }
        return false;
    }
}
exports.BuildService = BuildService;
//# sourceMappingURL=BuildService.js.map