"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const os = require("os");
const MESSAGE_START = 'LoggingSession started';
const FILE_EXTENSION = '.txt';
class ExtLog {
    get EnableLogging() {
        return this.m_EnableLogging;
    }
    set EnableLogging(i_Value) {
        this.m_EnableLogging = i_Value;
    }
    //base path for LogFile
    get BaseLogFilePath() {
        return this.m_BaseLogFilePath;
    }
    //actual filepath of logFile
    get LogFilePath() {
        return this.m_LogFilePath;
    }
    constructor(i_FilePath) {
        this.m_EnableLogging = false;
        this.m_BaseLogFilePath = i_FilePath;
        this.m_LogFilePath = this.BaseLogFilePath + ExtLog.GenerateLogTime() + FILE_EXTENSION;
        this.Log(MESSAGE_START);
    }
    static GenerateLogTime() {
        let date = new Date();
        return date.getHours() + '_' + date.getMinutes() + '_' + date.getSeconds() + '-' + date.getDate() + '_' + (date.getMonth() + 1) + '_' + date.getFullYear();
    }
    Log(i_Message) {
        if (this.EnableLogging === true) {
            fs.appendFileSync(this.LogFilePath, i_Message + os.EOL);
        }
    }
}
exports.ExtLog = ExtLog;
//# sourceMappingURL=ExtLog.js.map