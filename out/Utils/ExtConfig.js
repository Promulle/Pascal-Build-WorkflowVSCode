"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const vscode = require("vscode");
const extConst = require("../Const/extensionConstants");
class ExtConfig {
    GetSetting(i_Name, i_DefaultValue) {
        return vscode.workspace
            .getConfiguration(extConst.EXTENSION_PREFIX)
            .get(i_Name, i_DefaultValue);
    }
}
exports.ExtConfig = ExtConfig;
//# sourceMappingURL=ExtConfig.js.map