"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const os = require("os");
const path = require("path");
const log = require("./ExtLog");
class ExtUtils {
    constructor(i_LoggerFile) {
        this.Logger = new log.ExtLog(i_LoggerFile);
    }
    //reads from file to array of string
    ReadFileToArray(i_FilePath) {
        let lines = [];
        let read = fs.readFileSync(i_FilePath, 'utf8');
        lines = read.split(os.EOL);
        return lines;
    }
    //capitalizes first letter
    static CapitalizeFirstLetter(i_Value) {
        return i_Value.charAt(0).toUpperCase() + i_Value.slice(1);
    }
    static GetFilePaths(i_Directory) {
        let fileNames = fs.readdirSync(i_Directory);
        let res = [];
        let i;
        for (i = 0; i < fileNames.length; i++) {
            res.push(i_Directory + path.sep + fileNames[i]);
        }
        return res;
    }
}
exports.ExtUtils = ExtUtils;
//# sourceMappingURL=ExtUtils.js.map