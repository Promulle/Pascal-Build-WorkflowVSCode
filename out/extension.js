'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const vscode = require("vscode");
//imports for custom files
const extUtils = require("./Utils/ExtUtils");
const extConst = require("./Const/extensionConstants");
const extConfig = require("./Utils/ExtConfig");
const extBuildService = require("./Services/BuildService");
const extInsertService = require("./Services/InsertLineService");
let appConfig = new extConfig.ExtConfig();
let appUtils = new extUtils.ExtUtils(appConfig.GetSetting(extConst.SETTING_BASE_LOG_PATH, extConst.SETTING_BASE_LOG_PATH_DEFAULT));
let appBuildService = new extBuildService.BuildService(appConfig, appUtils);
let appInsertService = new extInsertService.InsertLineService(appConfig);
function activate(context) {
    appUtils.Logger.EnableLogging = appConfig.GetSetting(extConst.SETTING_ENABLE_LOGGING, extConst.SETTING_ENABLE_LOGGING_DEFAULT);
    let disposable = vscode.commands.registerCommand(extConst.COMMAND_BUILD_CURRENT, () => {
        appBuildService.CallSingleBuild();
    });
    let secondCommand = vscode.commands.registerCommand(extConst.COMMAND_BUILD_ALL, () => {
        appBuildService.CallAllBuild();
    });
    let insertCommand = vscode.commands.registerCommand(extConst.COMMAND_INSERT_LINE, () => {
        appInsertService.InsertLine();
    });
    context.subscriptions.push(disposable);
    context.subscriptions.push(secondCommand);
    context.subscriptions.push(insertCommand);
}
exports.activate = activate;
// this method is called when your extension is deactivated
function deactivate() {
}
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map